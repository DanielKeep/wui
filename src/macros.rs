#[macro_export]
macro_rules! boolish {
    (
        $(#[$($attrs:tt)*])*
        pub boolish $name:ident {
            $yes:ident = true,
            $no:ident = false $(,)*
        }
    ) => {
        boolish! {
            @as_items
            $(#[$($attrs)*])*
            pub enum $name {
                $yes,
                $no
            }

            impl From<bool> for $name {
                fn from(v: bool) -> Self {
                    match v {
                        true => $name::$yes,
                        false => $name::$no,
                    }
                }
            }

            impl From<$name> for bool {
                fn from(v: $name) -> Self {
                    match v {
                        $name::$yes => true,
                        $name::$no => false,
                    }
                }
            }
        }
    };

    (@as_items $($i:item)*) => { $($i)* };
}

#[macro_export]
macro_rules! wnd_proc {
    (
        unsafe extern "system" fn $wnd_proc:ident
        |> fn $try_wnd_proc:ident(
            $wnd:ident,
            $message:ident,
            $w_param:ident,
            $l_param:ident
        ) -> Result<_, $err_ty:ty> {
            $($body:tt)*
        }
    ) => {
        wui_util__! {
            @as_items
            fn $try_wnd_proc(
                $wnd: $crate::winapi_::HWND,
                $message: $crate::winapi_::UINT,
                $w_param: $crate::winapi_::WPARAM,
                $l_param: $crate::winapi_::LPARAM,
            ) -> ::std::result::Result<$crate::winapi_::LRESULT, $err_ty> {
                $($body)*
            }
        }

        unsafe extern "system" fn $wnd_proc(
            wnd: $crate::winapi_::HWND,
            message: $crate::winapi_::UINT,
            w_param: $crate::winapi_::WPARAM,
            l_param: $crate::winapi_::LPARAM,
        ) -> $crate::winapi_::LRESULT {
            use std::io;
            use std::panic;
            use std::result::Result::{Ok, Err};
            use std::string::String;

            match panic::recover(move || {
                $try_wnd_proc(wnd, message, w_param, l_param)
            }) {
                Ok(Ok(res)) => res,
                Ok(Err(err)) => wui_abort!(
                    "\
                        Unhandled error: {}\r\n\
                        \r\n\
                        wnd: {:p}\r\n\
                        message: {:?}\r\n\
                        w_param: 0x{:x}\r\n\
                        l_param: 0x{:x}\
                    ",
                    err, wnd, $crate::FormatMsg(message), w_param, l_param,
                ),
                Err(err) => {
                    let msg = if let Some(err) = err.downcast_ref::<&'static str>() {
                        String::from(*err)
                    } else if let Some(err) = err.downcast_ref::<String>() {
                        (*err).clone()
                    } else if let Some(err) = err.downcast_ref::<io::Error>() {
                        (*err).to_string()
                    } else {
                        String::from("(unknown)")
                    };

                    wui_abort!(
                        "\
                            Panic: {}\r\n\
                            \r\n\
                            wnd: {:p}\r\n\
                            message: {:?}\r\n\
                            w_param: 0x{:x}\r\n\
                            l_param: 0x{:x}\
                        ",
                        msg, wnd, $crate::FormatMsg(message), w_param, l_param
                    )
                },
            }
        }
    };
}

#[macro_export]
macro_rules! wui_abort {
    ($($args:tt)*) => {
        match format!($($args)*) {
            msg => $crate::wui_abort(&msg, None)
        }
    };
}

#[macro_export]
macro_rules! wui_no_panic {
    ($($body:tt)*) => {
        match ::std::panic::recover(move || wui_util__!(@as_expr {$($body)*})) {
            Ok(res) => res,
            Err(err) => wui_abort!("Panic: {}", err)
        }
    };
}

#[macro_export]
macro_rules! wui_ok_or_warn {
    ($($body:tt)*) => {
        match (|| -> ::std::result::Result<(), Box<::std::error::Error>> {
            wui_util__!(@as_expr { $($body)* })
        })() {
            Ok(()) => (),
            Err(err) => {
                use ::std::io::Write;
                let _ = writeln!(::std::io::stderr(), "Warning: ignored error: {}", err);
            }
        }
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! wui_util__ {
    (@as_expr $e:expr) => { $e };
    (@as_items $($i:item)*) => { $($i)* };
}

macro_rules! io_err {
    ($arg:expr) => {
        Err(::std::io::Error::new(::std::io::ErrorKind::Other, $arg))
    };

    ($arg:expr, $($tail:tt)*) => {
        Err(::std::io::Error::new(::std::io::ErrorKind::Other,
            format!($arg, $($tail)*)))
    };
}

macro_rules! IntoRepr {
    (
        ($repr_ty:ty) pub enum $name:ident $($_tail:tt)*
    ) => {
        impl $name {
            pub fn into_repr(self) -> $repr_ty {
                self as $repr_ty
            }
        }
    };
}
